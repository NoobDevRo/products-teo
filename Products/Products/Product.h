#pragma once
#include <string>
#include <cstdint>
#include "IPriceable.h"

class Product :public IPriceable
{
public:
	Product(int32_t id, const std::string& name, float rawPrice);

	uint16_t getID() const;
	const std::string getName() const;
	float getRawPrice() const;

protected:
	int32_t m_id;
	std::string m_name;
	float m_rawPrice;
};