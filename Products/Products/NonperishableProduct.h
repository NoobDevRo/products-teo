#pragma once
#include "Product.h"



class NonperishableProduct :public Product
{
public:

	enum class Type
	{
		Clothing = 1,
		SmallAppliences = Clothing << 1,
		PersonalHygiene = SmallAppliences << 1
	};


public:
	NonperishableProduct(int32_t id, const std::string& name, float rawPrice, Type& type);
	~NonperishableProduct() override = default;
	Type getType() const;
	float getPrice() const override final;
	int32_t getVAT() const override final;

private:
	Type m_type;
};